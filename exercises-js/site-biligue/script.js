<script>
window.onload = function() {
    var url = 'http://www.meulink.com/'; // página default
    var lang = window.navigator.language;
    console.log('idioma: ', lang);

    if (lang == 'pt-BR' || lang == 'pt_BR') {
        url = 'http://www.meulink.com/pt/';
    } else if (lang == 'es' || lang == 'es-ES' || lang == 'es_ES') {
        url = 'http://www.meulink.com/es/';
    }
    window.location.href = url; // redireciona
};
</script>
/**
Esse trecho de código você pode colocar na tag head ou antes do final de body…
Deixei a linha do console.log para te ajudar a testar o comportamento, use F12 do seu navegador na aba console e veja o que ele imprime e ajuste o if conforme necessidade…
Apenas lhe mostrei um exemplo, pesquise mais a respeito…
Se quiser se aprofundar em javascript (recomendo muito), estude a API de i18n (internationalization ou internacionalização) Intl:
https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Intl
**/